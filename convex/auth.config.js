export default {
  providers: [
    {
      domain: "https://magical-coral-19.clerk.accounts.dev",
      applicationID: "convex",
    },
  ],
};
